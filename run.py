import sys
import json
import os 


MIN_PYTHON = '3.4'
CONFIG_FILE = 'config.json'

# Checking Python 3.4
print('Checking Python version... ', end = '')
version = MIN_PYTHON.split('.')
if sys.version_info[0] < int(version[0]) and sys.version_info[1] < int(version[1]):
    print('Failed')
    print ('Python version must be at least ' + MIN_PYTHON)
    sys.exit()
print('OK')

# Import mutcrawler
print('Checking mutcrawler package... ', end = '')
try:
    from mutcrawler import *
except ImportError as e:
    print('Failed')
    print('Error: ' + str(e))
    print('mutcrawler may not be installed\nPlease run \"pip install .\\\" in src folder')
    sys.exit()
print('OK')

# Asking for start
print('Please check config.json before start mutcrawler!')
selection = input('Start mutcrawler? (y/n) ')
if selection != 'y' and selection != 'Y':
    sys.exit()

# Read config file
print('Loading config file... ', end = '')
try:
    with open(CONFIG_FILE) as file:
        config = json.load(file)
except IOError:
    print('Failed')
    print(CONFIG_FILE + ' not found')
    sys.exit()
except:
    print('Failed')
    print('Cannot load ' + CONFIG_FILE)
    sys.exit()
print('OK')

# Run mutcrawler
print('Start crawler...')
path = os.path.dirname(os.path.realpath(__file__))
main.start(config, path)
