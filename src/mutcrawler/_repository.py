import os

# This class can be changed after the files' structure are confirmed
class Repository(object):

    def __init__(self, filePath):
        self.filePath = filePath  # the data file to write
        self.openned = False

    # Store the url and content into the file
    def save_data(self, url, text):
        try:
            if self.openned:
                content = url + "\t" + text + "\n"
                self.file.write(content)
                return True
        except:
            return False

    # open file to create new file and write
    def open_write(self):
        try:
            os.makedirs(os.path.dirname(self.filePath), exist_ok=True)
            self.file = open(self.filePath, "w", encoding='utf8')
            self.openned = True
            return True
        except:
            return False

    # close files
    def close(self):
        try:
            self.file.close()
            self.openned = False
            return True
        except:
            return False

    # Check if file is closed
    def isClosed(self):
        return self.openned
