import sys
import time
import urllib
import urllib.error
import urllib.request
from urllib.parse import urlparse
from cgi import escape
from bs4 import BeautifulSoup

AGENT = 'mutCrawler'


# Helper recognite opaque data exception
class OpaqueDataException (Exception):
	def __init__(self, message, mimetype, url):
		Exception.__init__(self, message)
		self.mimetype=mimetype
		self.url=url


# Helper class to retrieve and interpret web page
class Fetcher(object):

	def __init__(self, url):
		self.url = url
		
		self.all_urls = []
		self.text = 'Content not found'

	# Get x-th url in out_urls list
	def __getitem__(self, x):
		return self.all_urls[x]

	# Return all urls in page
	def all_links(self):
		return self.all_urls

	# Return content in page
	def get_content(self):
		return self.content

	# Add header
	def _add_headers(self, request):
		request.add_header("User-Agent", AGENT)

	# Open url
	def _open(self):
		url = self.url

		request = urllib.request.Request(url)
		handle = urllib.request.build_opener()

		return (request, handle)

	# Fetch page
	def fetch(self):
		request, handle = self._open()
		self._add_headers(request)

		if handle:
			invisible_tags = []

			data = handle.open(request, timeout = 10)
			mime_type = data.info().get_content_type()

			url = data.geturl()

			if mime_type != 'text/html':
				raise OpaqueDataException("Not interested in files of type %s" % mime_type,
																			mime_type, url)

			content = data.read().decode('utf-8', errors='replace')
			soup = BeautifulSoup(content, 'html.parser')
			tags = soup('a')

			invisible_tags = soup.find_all(['style', 'script'])

			# Get links from page
			for tag in tags:
				href = tag.get('href')
				if href is not None:
					url = urllib.parse.urljoin(self.url, escape(href))
					if url not in self:
						self.all_urls.append(url)

			# Remove invisible tags from soup
			for invisible_tag in invisible_tags:
				invisible_tag.extract()

			# Get content
			# Remove newline, tab character to prevent issues when save file csv
			self.content = ' '.join(soup.stripped_strings).replace('\r\n', ' ').replace('\n', ' ').replace('\t', ' ')
