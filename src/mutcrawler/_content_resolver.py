import sys
import time
import urllib
import urllib.error
import urllib.request
from urllib.parse import urlparse
from sys import getsizeof
from threading import Timer
import requests

from bs4 import BeautifulSoup
from . import _page_fetch
from . import _url_filter
from . import _repository
from . import _constant as const


# This class will be assigned to a thread as a task to crawl an url
class ContentResolver(object):
	def __init__(self, fileName):
		self.page = None
		self.repo = _repository.Repository(fileName)

	# Get list links from a page
	def extract_urls(self, num_link, max_url_length):
		if self.page == None:
			return None

		urls = self.page.all_links()

		url_filter = _url_filter.FilterURL(urls=urls, max_length=max_url_length)
		urls_ok = url_filter.get_okurls()

		# If urls_ok list is not enough num_link, return all list
		if len(urls_ok) <= num_link:
			return urls_ok

		# Else return num_link first urls in list
		return urls_ok[:num_link]

	# Get content of url
	def extract_text(self):
		if self.page == None:
			return None

		return self.page.get_content()

	# Fetch html and parse content
	def fetch(self, url):
		self.page = _page_fetch.Fetcher(url)
		self.page.fetch()

	# Open data file to write
	def begin(self):
		return self.repo.open_write()

	# Close data file
	def end(self):
		if self.repo.isClosed() is True:
			self.repo.close()

	# Perform crawler, crawlerWorker will call this in run function
	def perform(self, id, args):
		seedUrl = args[const.URL]
		level = args[const.LEVEL]
		maxUrlNum = args[const.MAX_URL_PER_PAGE]
		maxUrlLength = args[const.MAX_URL_LENGTH]

		self.fetch(seedUrl)
		urls = self.extract_urls(maxUrlNum, maxUrlLength)
		text = self.extract_text()
		
		self.repo.save_data(seedUrl, text)

		response = {
			const.URL: seedUrl,
			const.EXTRACTED_URLs: urls,
			const.LEVEL: level + 1,
			const.DATA_SIZE: len(text.encode('utf8'))
		}
		return response
