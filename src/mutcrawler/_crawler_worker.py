from threading import Thread
from threading import Event
import time

# Worker thread for crawling
class CrawlerWorker(Thread):
    def __init__(self, threadId, callback):
        self.id = threadId
        self.callback = callback
        self.stopped = True
        self.task = None
        self.args = None
        self.delay = 0
        self.handler = Event()
        Thread.__init__(self, target = self.doTask,  args = (self.handler,))

    # Assign a new task to worker thread
    def setTask(self, task):
        self.task = task

    # Set new arguments
    def newArgs(self, args):
        self.args = args

    # Start thread, this function should be call one time
    def startDelay(self, delay):
        self.stopped = False
        self.handler.set()
        self.delay = delay
        super(CrawlerWorker, self).start()

    # Resume a thread after assign new task
    def resume(self):
        self.handler.set() # unlock Event

    # Entry point of a thread
    # Code in this function run on separate thread
    def doTask(self, event):
        time.sleep(self.delay)
        while not self.stopped:
            try:
                response = self.task.perform(self.id, self.args)
                self.pause()
                self.callback.onTaskComplete(self.id, response)
            except Exception as e:
                self.pause()
                self.callback.onTaskError(self.id, str(e))

            if not self.stopped:
                event.wait() # Block until call self.handler.set()

    # Pause a worker thread after it has done the current task
    def pause(self):
        self.handler.clear()

    # Request a thread shutdown (cannot stop current thread immediately)
    def shutDown(self):
        self.stopped = True
        self.handler.set()

    # Check if a thread is available for new task
    def isWaiting(self):
        return not self.handler.is_set() or self.stopped
