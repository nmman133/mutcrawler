import os
import datetime
from threading import Timer
from threading import Lock
from queue import Queue

from . import _config_loader
from . import _crawler_worker
from . import _frontier_manager
from . import _content_resolver
from . import _constant as const
from . import _utilities as util

DATA_FOLDER = 'data'


# Manage all crawler thread
class CrawlerManager:
    def __init__(self, config, path):
        self.config = _config_loader.ConfigLoader(config)
        self.path = path

        # Init frontier
        self.frontier = _frontier_manager.Frontier(self.config.getFrontierSize(), self.config.getException())
        self.frontier.addSeed(self.config.getSeed())

        # Init threads and resolvers
        self.threadList = []
        for i in range(self.config.getNumThread()):
            fileName = const.DATA_FILE_PREFIX + str(i) + '.' + const.DATA_FILE_EXTENTION # Data file name
            dataFolder = os.path.join(path, DATA_FOLDER)
            worker = _crawler_worker.CrawlerWorker(i, self)  # self here is a callback
            resolver = _content_resolver.ContentResolver(os.path.join(dataFolder, fileName))
            item = (worker, resolver)
            self.threadList.append(item)

        self.totalPage = 0
        self.totalDataSize = 0
        self.hashTable = {}
        self.printLock = Lock()
        self.eventSender = Queue()
        self.clock = Timer(self.config.getPeriodTimeout(), self.onTimeOut)
        self.num_period = 1
        self.finishing = False

        for seed in self.config.getSeed():
            self.hashTable[seed] = 0

        if self.config.getMaxData() > 0:
            self.MAX_BYTE = self.config.getMaxData() * (1024*1024)
        else:
            self.MAX_BYTE = -1

    # Entry point of crawler
    def start(self):
        if self.frontier.isEmpty():
            util.printError('Frontier is empty')
            return

        for item in self.threadList:
            worker = item[0]
            resolver = item[1]
            worker.setTask(resolver)
            if self.assign(worker):
                self.startThread(worker, resolver)
        
        self.clock.start()
        self.loop()

    # Main loop to wait for child thread response
    def loop(self):
        while True:
            if self.checkDeadlock():
                break

            event = self.eventSender.get() # Block until queue has an item

            if event[const.EVENT_CODE] == const.EVENT_TASK_SUCCESS:
                threadId = event[const.EVENT_THREAD_ID]
                msg = event[const.EVENT_BODY][const.URL]
                with self.printLock:
                    print('Thread' + str(threadId) + ': ' + msg)
                allow = self.handleResult(event[const.EVENT_BODY])
                if not allow:
                    self.finishing = True
                    break
            elif event[const.EVENT_CODE] == const.EVENT_SHUTDOWN:
                threadId = event[const.EVENT_THREAD_ID]
                msg = event[const.EVENT_BODY]
                with self.printLock:
                    util.printError('Thread' + str(threadId) + ': ' + msg)
                self.finishing = True
                break

            self.recycle() # Reassign task for threads
        
        self.shutDown()

    # Reassign task for threads
    def recycle(self):
        for item in self.threadList:
            worker = item[0]
            success = self.assign(worker)
            if success:
                if worker.is_alive():
                    worker.resume()
                else:
                    resolver = item[1]
                    self.startThread(worker, resolver)

    # Start a thread
    def startThread(self, worker, resolver):
        util.printInfo('Thread' + str(worker.id) + ' preparing...')
        resolver.begin()
        worker.startDelay(const.THREAD_STARTUP_DELAY)

    # Set new argument to a thread
    def assign(self, worker):
        if worker.isWaiting() and not self.frontier.isEmpty():
            item = self.frontier.pop()
            args = {
                const.URL: item[const.URL],
                const.LEVEL: item[const.LEVEL],
                const.MAX_URL_PER_PAGE: self.config.getMaxUrlPerPage(),
                const.MAX_URL_LENGTH: self.config.getMaxUrlLength()
            }
            worker.newArgs(args)
            return True
        return False

    # Handle response from a thread
    def handleResult(self, response):
        self.totalPage = self.totalPage + 1
        self.totalDataSize = self.totalDataSize + response[const.DATA_SIZE]

        limitPage = self.config.getMaxPage()
        if limitPage > 0 and self.totalPage == limitPage:
            with self.printLock:
                util.printError('Maximun number of crawled pages')
            return False

        if self.MAX_BYTE > 0 and self.totalDataSize > self.MAX_BYTE:
            with self.printLock:
                util.printError('Maximun data size')
            return False

        # Add extracted urls to frontier
        level = response[const.LEVEL]
        if level < self.config.getCrawlDepth():
            urls = response[const.EXTRACTED_URLs]
            for url in urls:
                if url in self.hashTable:
                    continue
                if self.frontier.push(url, level):
                    self.hashTable[url] = 0
        
        return True

    # Build event for putting to queue
    def buildEvent(self, id, code, body):
        event = {
            const.EVENT_THREAD_ID: id,
            const.EVENT_CODE: code,
            const.EVENT_BODY: body
        }
        return event

    # Build shutdown event for putting to queue
    def buildShutdownEvent(self):
        event = {
            const.EVENT_THREAD_ID: -1,
            const.EVENT_CODE: const.EVENT_SHUTDOWN,
            const.EVENT_BODY: None
        }
        return event

    # Check if all threads are waiting and frontier is empty
    def checkDeadlock(self):
        waitingThreads = 0
        for item in self.threadList:
            worker = item[0]
            if not worker.isWaiting():
                return False
            else:
                waitingThreads += 1
        frontierSize = self.frontier.getSize()
        if frontierSize == 0:
            with self.printLock:
                util.printError('DEADLOCK: frontier is empty')
            return True
        return False

    # Callback for a thread complete its task (call async)
    def onTaskComplete(self, id, response):
        event = self.buildEvent(id, const.EVENT_TASK_SUCCESS, response)
        self.eventSender.put(event)
        
    # Callback when a thread has error (call async)
    def onTaskError(self, id, response):
        event = self.buildEvent(id, const.EVENT_TASK_FAIL, response)
        self.eventSender.put(event)
     
    # Shutdown web crawler
    def shutDown(self):
        util.printError("Crawler shutdown...")

        self.clock.cancel()

        # Request all threads shutdown
        for item in self.threadList:
            worker = item[0]
            resolver = item[1]
            if worker.is_alive():
                worker.shutDown()
                util.printInfo("Waiting for thread " + str(worker.id) + ' join...')
                worker.join() # Waiting until this thread done its current task
                resolver.end()

        util.printError("Terminated!")
        self.logStatistics(True)
        self.printInfo()

    # Timeout callback
    def onTimeOut(self):
        with self.printLock:
            util.printError("End preriod " + str(self.num_period))

        if self.num_period == self.config.getNumPeriod():
            with self.printLock:
                util.printError('Timeout!')
            self.eventSender.put(self.buildShutdownEvent())
            return

        self.logStatistics()
        self.num_period += 1
        self.clock = Timer(self.config.getPeriodTimeout(), self.onTimeOut)
        self.clock.start()

    # Print statistics when finished
    def printInfo(self):
        print("==== Information ====")
        print("Crawled pages: " + str(self.totalPage))
        print("Total data: %.2f MB" % float(self.totalDataSize / (1024*1024)))
        print("Output folder: " + os.path.join(self.path, DATA_FOLDER))
        print("=====================")

    # Write statistics to file
    def logStatistics(self, last = False):
        pages = str(self.totalPage)
        data = "%.2f MB" % float(self.totalDataSize / (1024*1024))
        period = str(self.num_period)

        fileName = const.STAT_FILE_NAME + '.' + const.STAT_FILE_EXTENTION
        path = os.path.join(self.path, fileName)

        with open(path, "a", encoding='utf8') as f: 
            f = open(path, "a", encoding='utf8')
            content = datetime.datetime.now().strftime("=== Period " + period  \
                + " | %Y-%m-%d %H:%M:%S ===\nCrawled pages: " + pages + "\nTotal data: " + data + "\n")
            if last:
                content += "\n---\n\n"
            f.write(content)
