import multiprocessing

# Helper class to get config values
class ConfigLoader:
    def __init__(self, config):
        self.config_crawler = config['crawler']
        self.config_frontier = config['frontier']
        self.config_url = config['url']
        self.config_seed = config['seed']
        self.config_exception = config['exception']

    # Get the max number of threads, if config is 'auto' return the number of cpus
    def getNumThread(self):
        value = self.config_crawler['num_thread']
        if (value == 'auto'):
            return multiprocessing.cpu_count()
        return int(value)

    # Get crawler depth graph limit
    def getCrawlDepth(self):
        return int(self.config_crawler['crawl_depth'])

    # Get max limit of page
    def getMaxPage(self):
        return int(self.config_crawler['max_page'])

    # Get max limit of data size
    def getMaxData(self):
        return int(self.config_crawler['max_data'])

    # Get period timeout
    def getPeriodTimeout(self):
        return int(self.config_crawler['period_timeout'])

    # Get number of periods
    def getNumPeriod(self):
        return int(self.config_crawler['num_period'])

    # Get frontier size
    def getFrontierSize(self):
        return int(self.config_frontier['frontier_size'])

    # Get max limit of url length
    def getMaxUrlLength(self):
        return int(self.config_url['max_url_length'])

    # Get max limit of url per page
    def getMaxUrlPerPage(self):
        return int(self.config_url['max_url_per_page'])

    # Get list of seeds
    def getSeed(self):
        return self.config_seed

    # Get list of exceptions
    def getException(self):
        return self.config_exception
