import sys
import optparse
from colorama import init 
from termcolor import colored 
  

init()

USAGE = '%prog [option] <url>'
VERSION = '%prog v1'

def parse_options(depth_limit):
	parser = optparse.OptionParser(usage=USAGE, version=VERSION)

	parser.add_option('-q', '--quiet',
						action='store_true', default=False, dest='quiet',
						help='Enable quite mode')

	parser.add_option('-l', '--links',
						action='store_true', default=False, dest='links',
						help='Get links for specified url only')

	parser.add_option('-d', '--depth',
						action='store', type='int', default=depth_limit, dest='depth_limit',
						help='Maximum depth to traverse')

	parser.add_option('-c', '--confine',
						action='store', type='string', dest='confine',
						help='Confine crawl to specified prefix')

	parser.add_option('-x', '--exclude',
						action='append', type='string', dest='exclude',
						default=[], help='Exclude URLs by prefix')

	parser.add_option('L', '--show-links',
						action='store_true', default=False, dest='out_links',
						help='Output URLs found')

	parser.add_option('-u', '--show-urls',
						action='store_true', default=False, dest='out_urls',
						help='Output URLs found')

	parser.add_option('-D', '--dot',
						 action='store_true', default=False, dest='out_dot',
						 help='Output Graphviz dot file')

	opts, args = parser.parse_args()


	if len(args) < 1:
		parser.print_help(sys.stderr)
		raise SystemExit(1)


	if opts.out_links and opts.out_urls:
		parser.print_help(sys.stderr)
		parser.error('options -L and -u are mutually exclusive')


	return opts, args


def printError(msg):
	print(colored(msg, 'red'))

def printSuccess(msg):
	print(colored(msg, 'green'))

def printInfo(msg):
	print(colored(msg, 'yellow'))