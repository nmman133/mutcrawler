from . import _config_loader
from . import _crawler_manager

# Start crawler
def start(config, path):
    crawlerManager = None
    try:
        crawlerManager = _crawler_manager.CrawlerManager(config, path)
        crawlerManager.start()
    except Exception as e:
        print('Error: ' + str(e))
        if crawlerManager:
            crawlerManager.shutDown()
    
