# Arguments
MAX_URL_LENGTH = "max_url_length"
MAX_URL_PER_PAGE = "max_url_per_page"
URL = "url"
LEVEL = "level"
IGNORE_LIST = "ignore_list"

# Response
EXTRACTED_URLs = "extracted_url"
DATA_SIZE = "data_size"

# Data file
DATA_FILE_PREFIX = "data_thread"
DATA_FILE_EXTENTION = "mut"

# Statistics file
STAT_FILE_NAME = "statistics"
STAT_FILE_EXTENTION = "txt"

# Event code
EVENT_TASK_SUCCESS = 0
EVENT_TASK_FAIL = 1
EVENT_SHUTDOWN = -1

# Event arg
EVENT_CODE = 'code'
EVENT_THREAD_ID = 'thread_id'
EVENT_BODY = 'body'

# Thread startup delay
THREAD_STARTUP_DELAY = 2
