from . import _constant as const


# Url stack for DFS crawler
class Frontier:
    def __init__(self, maxSize, ignoreList):
        self.maxSize = maxSize
        self.ignoreList = ignoreList
        self.stack = []

    # Add seed urls to stack
    def addSeed(self, seeds):
        for url in seeds:
            self.push(url, 0)

    # Push to stack
    def push(self, url, level):
        for ignoreUrl in self.ignoreList:
            if ignoreUrl == url or ignoreUrl in url:
                return
        item = {
            const.URL: url,
            const.LEVEL: level
        }
        if not self.isFull():
            self.stack.append(item)
            return True
        return False

    # Check stack is empty or not
    def isEmpty(self):
        return len(self.stack) < 1

    # Check stack is full or not
    def isFull(self):
        return len(self.stack) == self.maxSize

    def getSize(self):
        return len(self.stack)

    # Pop and return the item from stack
    def pop(self):
        if self.isEmpty() is False:
            lastId = len(self.stack) - 1
            last = self.stack[lastId]
            self.stack.pop()
            return last
        else:
            return None
