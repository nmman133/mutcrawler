from setuptools import setup

# List of dependencies using in this package
DEPENDENCIES = [
    'beautifulsoup4',
    'requests',
    'colorama',
    'termcolor'
]

# Install package
setup(name = 'mutcrawler',
      version = '1.0.3',
      description = 'Web of science midterm project',
      author = 'Nguyen Minh Man | Phan Thi Phuong Uyen | Huynh Ngoc Thanh Tung',
      author_email = 'minhman133@outlook.com | ptpuyen1511@gmail.com | huynhngocthanhtung@gmail.com',
      license = 'MIT',
      packages = ['mutcrawler'],
      install_requires = DEPENDENCIES,
      zip_safe = False)
