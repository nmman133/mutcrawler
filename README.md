# mutcrawler
Đồ án môn Khoa học về Web lớp CQ2015/2

### Thông tin nhóm:
1. Nguyễn Minh Mẫn
2. Phan Thị Phương Uyên
3. Huỳnh Ngọc Thanh Tùng
> mutcrawler là viết tắt của Mẫn-Uyên-Tùng web crawler

### Quy trình làm việc
1. Thiết kế kiến trúc *(done, 6/10)*
2. Tổ chức Repository *(done, 8,10)*
3. Lập trình (in proccessing) *(done, 1/11)*
4. Kiểm thử và sửa lỗi *(done, 4/11)*
5. Thực hiện yêu cầu 1 của đồ án (in proccessing)
6. Viết báo cáo (in proccessing)
7. Nộp bài

### Tài nguyên
1. [Sơ đồ kiến trúc](https://www.lucidchart.com/invitations/accept/55eaebac-87c9-426d-a3ea-60e16f7dd9cd)

### Hướng dẫn quy trình làm việc trên git
**1. Cài git và cấu hình lần đầu**
+ Tải và cài đặt git từ [trang chủ](https://git-scm.com/downloads)
+ [Cấu hình git lần đầu](https://git-scm.com/book/vi/v1/B%E1%BA%AFt-%C4%90%E1%BA%A7u-C%E1%BA%A5u-H%C3%ACnh-Git-L%E1%BA%A7n-%C4%90%E1%BA%A7u)

**2. Clone project về máy tính:**
+ Đến một thư mục nào đó
+ Clone project về máy tính:

  > git clone https://github.com/minhman318/mutcrawler.git
  
+ Chuyển sang nhánh develop (mình sẽ không code trực tiếp trên nhánh master):

  > git checkout develop
  
**3. Trước khi bắt đầu code:**
+ Kiểm tra mình đang đứng ở nhánh nào:

  > git status
  
+ Cập nhật thay đổi từ nhánh develop trên remote:

  > git pull origin develop

**4. Sau khi đã sửa một số file:**
+ Nếu có tạo hay xóa file, phải thêm nó vào git bằng lệnh:

  > git add <tên file>
  
  hoặc để thêm tất cả file:
  
  > git add . 
  
+ Kiểm tra các file đã sửa hay thêm:

  > git status
  
+ Commit các thay đổi:

  > git commit -m "thông điệp"
  
*Thông điệp nên là một câu ngắn gọn mô tả những thay đổi của mình trong commit đó*
  
+ Cập nhật lại thay đổi từ remote trước khi push:

  > git pull --rebase origin develop (1)
  
*Lệnh pull --rebase nghĩa là lấy bản mới nhất từ remote, sau đó áp tất cả các commit local của mình vào đó. Nếu lệnh pull không có --rebase, nó
mặc định sẽ tạo thêm 1 commit merge hai commit mới nhất giữa local và remote với nhau.*
  
+ Đẩy các commit lên nhánh develop trên remote

  > git push origin develop

**5. Xử lý conflict nếu có:**

(1) Đây là lúc có thể xảy ra conflict, khi mà trên remote đã thay đổi trúng những file mà mình mới commit ở local.
Nếu bị conflict, git sẽ báo những file nào bị conflict, những chỗ conflict sẽ bị đánh dấu, mình phải vào sửa tay từng file conflict.
+ Sau khi mình sửa xong hết thì thêm các file vào lại git:

  > git add .

+ Sau đó mình tiếp tục rebase:

  > git rebase --continue
  
+ Lúc này mình có thể tiếp túc bước push như bình thường
